# **Taxonomy Pattern**

## Beschreibung

Das **Taxonomy Pattern** erlaubt es, dass beliebige Objekte in einer monohierarchische Struktur geordnet werden, obwohl die Objekte völlig verschieden sind. Ein Objekt kann maximal ein oder kein Elternobjekt besitzen, wordurch die Verbindungen der Objekte eine Baumstruktur bildet.

Aus Wikipedia:

> Als Taxonomie in der Informationsverarbeitung werden Klassifikationen bezeichnet, die eine monohierarchische Struktur aufweisen. Dabei wird jeder Klasse nur eine Oberklasse zugeordnet, so dass die gesamte Klassifikation eine Baumstruktur abbildet. In dieser Struktur enthalten die der Wurzel nahestehenden Elemente allgemeine Informationen. Mit einer zunehmenden Verzweigung der Taxonomie wird das darin hinterlegte Wissen immer spezifischer. Durch diese Art der Klassifizierung von Wissensbereichen innerhalb einer Hierarchie entsteht eine einfache Semantik.

### Beispiel

Ein `Post` kann nur einen `User` als Autor haben, also ist der `User` das Elternobjekt und der `Post` ein Kindobjekt.

- `User <- Post`

Ein `Post` kann aber auch Kommentare haben, wobei ein `Kommentar` nur zu einem einzigen `Post` gehören kann. Ein `Kommentar` kann aber auch zu einem `Bild` abgegeben werden. Das Kindobjekt `Kommentar` kann also einen `Post` oder ein `Bild` als Elternobjekt haben.

- `Post <- Kommentar`
- `Bild <- Kommentar`

Ein `Kommentar` selber könnte auch ein `Bild` enthalten. Damit wäre der `Kommentar` das Elternobjekt für das Kindobjekt `Bild`.

- `Kommentar <- Bild`

Die verschiedenen Objekte lassen sich verketten, wodurch eine Struktur bis zu einem Elternobjekt erstellt werden kann, das kein Elternobjekt mehr besitzt.

- `User <- Post <- Kommentar <- Bild`

Einige Objekte müssen immer einem Elternobjekt zugewiesen sein. Andere Objekte können auch keine Elternobjekte haben. Dies ist Teil der Implementierung und wird vom **Taxonomy Pattern** nicht abgedeckt.

Das **Taxonomy Pattern** kümmert sich nur darum, ob ein Objekt ein Elternobjekt besitzt und um nötige Daten zu liefern, damit Kindobjekte ein Objekt als Elternobjekt einsetzen und später wiederfinden können. Das **Taxonomy Pattern** sorgt NICHT dafür, dass die Kinderobjekte eines Objekts gefunden werden, da dies Teil der Implementierung ist, die sehr unterschiedlich ausfallen kann.

## Identifizierung

Ein Objekt muss durch einen Typen und eine ID eindeutig identifizierbar sein. Beide Angaben sind als `string` anzugeben und können beliebige Werte mit bis zu 255 Zeichen enthalten.

Wie ein Objekt die Referenz zu seinem Elternobjekt erhält und ablegt ist Teil der Implementierung. Ein Datenbank-Objekt könnte Type und ID des Elternobjekts separat speichern, oder diese Daten zusammengefasst (z.B. `post:12345`) speichern. 

## Interfaces

### TaxonomicInterface

Dieses Interface ist die Grundlage des **Taxonomy Pattern** und liefert den Type und die ID, mit dene dieses Objekt immer wieder gefunden werden kann.

+ getTaxonomicType: `string` - zum Beispiel `post`
+ getTaxonomicId: `string` - zum Beispiel `123456`
+ getTaxonomicParent: `TaxonomicInterface` - Diese Methode MUSS ein `TaxonomicInterface` zurückliefern. Wenn es Probleme beim Finden des Elternobjekts gibt, dann muss eine `TaxonomicException` geworfen werden.
+ hasTaxonomicParent: `boolean` - `true`, wenn ein Elternobjekt geladen werden kann, ansonsten `false`. Wenn diese Methode `true` liefert, dann darf `getTaxonomicParent()` keine `TaxonomicException` werfen, sondern muss das Elternobjekt liefern (eager loading).

## Exceptions

### TaxonomicException (extends Exception)

Wird geworfen, wenn es ein Problem beim Laden eines Elternjobektes gab.